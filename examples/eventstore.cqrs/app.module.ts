import { Module, OnModuleInit } from '@nestjs/common'
import { PersonsModule } from './persons/persons.module'
import { ConfigModule, ConfigService } from '@nestjs/config'
import { EventBusProvider, EventStoreCqrsModule, EventStoreSubscriptionType } from '../../src/'
import EventStoreConfig from './config/eventstore'
import { PersonAggregate } from './persons/models/person.aggregate'

@Module({
  imports: [
    ConfigModule.forRoot({
      load: [EventStoreConfig],
      isGlobal: true // if not done, configservice is not available for injection in the factory below
    }),
    EventStoreCqrsModule.forRootAsync(
      {
        inject: [ConfigService],
        useFactory: async (config: ConfigService) => {
          return {
            connectionSettings: config.get('connectionSettings'),
            endpoint: config.get('tcpEndpoint')
          }
        }
      },
      {
        onlyAckWhenHandlerIsDone: true,
        subscriptions: [
          {
            type: EventStoreSubscriptionType.PERSISTENT,
            stream: '$ce-persons',
            synchronize: true,
            persistentSubscriptionName: 'persons'
          }
        ],
        rollingSnapshots: [{ name: 'persons', everyNEvents: 1, aggegrate: PersonAggregate }]
      }
    ),
    PersonsModule
  ]
})
export class AppModule implements OnModuleInit {
  constructor(private _eventBus: EventBusProvider) {}

  onModuleInit() {
    console.log(this._eventBus.publisher.isLive)
  }
}
