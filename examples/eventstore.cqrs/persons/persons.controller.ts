import { Controller, Get, Param, Body, Post, Delete } from '@nestjs/common'
import { IPerson } from './models/person.model.interface'
import { CommandBus, QueryBus } from '@nestjs/cqrs'
import { AddPersonCommand } from './commands/impl/add-person.command'
import { DeletePersonCommand } from './commands/impl/delete-person.command'
import { ReadPersonQuery } from './queries/impl/read-person.query'

@Controller('persons')
export class PersonsController {
  constructor(private _commandBus: CommandBus, private _queryBus: QueryBus) {}

  @Get('/:id')
  async read(@Param('id') id: string): Promise<IPerson> {
    return this._queryBus.execute(new ReadPersonQuery(id))
  }

  @Post()
  async add(@Body() object: IPerson): Promise<void> {
    return this._commandBus.execute(new AddPersonCommand(object))
  }

  @Delete('/:id')
  async delete(@Param('id') id: string): Promise<void> {
    return this._commandBus.execute(new DeletePersonCommand(id))
  }
}
