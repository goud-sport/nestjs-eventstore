import { EntityRepository, MongoRepository } from 'typeorm'
import { ConflictException, InternalServerErrorException, NotFoundException } from '@nestjs/common'
import { InsertResult } from 'typeorm/query-builder/result/InsertResult'
import { PersonEntity } from '../models/person.entity'
import { DeleteWriteOpResultObject, UpdateWriteOpResult } from 'typeorm/driver/mongodb/typings'

@EntityRepository(PersonEntity)
export class MongoDbRepository extends MongoRepository<PersonEntity> {
  constructor() {
    super()
  }

  async insert(entity: PersonEntity): Promise<InsertResult> {
    return await super.insert(entity).catch(err => {
      if (err.code === 11000) throw new ConflictException("Entity already exists")
      else throw new InternalServerErrorException()
    })
  }

  async updateById(person: Partial<PersonEntity>): Promise<UpdateWriteOpResult> {
    return this.updateOne({ _id: person._id }, { $set: person }).then(obj => {
      if (!obj) throw new NotFoundException("Entity does not exist!")
      else return obj
    })
  }

  async deleteById(person: Partial<PersonEntity>): Promise<DeleteWriteOpResultObject> {
    return this.deleteOne({ _id: person._id }).then(obj => {
      if (obj.deletedCount === 0) throw new NotFoundException("Entity does not exist!")
      else return obj
    })
  }

  async findById(id: string): Promise<PersonEntity> {
    return this.findOne({ _id: id }).then(obj => {
      if (!obj) throw new NotFoundException("Entity does not exist!")
      else return obj
    })
  }
}