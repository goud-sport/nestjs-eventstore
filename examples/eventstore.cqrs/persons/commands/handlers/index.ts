import { AddPersonCommandHandler } from './add-person.command.handler'
import { DeletePersonCommandHandler } from './delete-person.command.handler'

export const PERSON_COMMAND_HANDLERS = [AddPersonCommandHandler, DeletePersonCommandHandler]
