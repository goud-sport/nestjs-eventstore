import { Module } from '@nestjs/common'
import { PersonsController } from './persons.controller'
import { PERSON_QUERY_HANDLERS } from './queries/handlers'
import { PERSON_EVENT_HANDLERS } from './events/handlers'
import { PERSON_COMMAND_HANDLERS } from './commands/handlers'
import { EventStoreRepository } from '../../../src'

@Module({
  controllers: [PersonsController],
  providers: [
    EventStoreRepository,
    ...PERSON_QUERY_HANDLERS,
    ...PERSON_COMMAND_HANDLERS,
    ...PERSON_EVENT_HANDLERS,
  ]
})
export class PersonsModule {}
