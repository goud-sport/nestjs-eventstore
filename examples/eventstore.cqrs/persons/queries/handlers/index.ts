import { ReadPersonHandler } from './read-person.query.handler'

export const PERSON_QUERY_HANDLERS = [ReadPersonHandler]
