import { QueryHandler, IQueryHandler } from '@nestjs/cqrs'
import { ReadPersonQuery } from '../impl/read-person.query'
import { Logger } from '@nestjs/common'
import { EventStoreRepository } from '../../../../../src'
import { PersonAggregate } from '../../models/person.aggregate'
import { IPerson } from '../../models/person.model.interface'

@QueryHandler(ReadPersonQuery)
export class ReadPersonHandler implements IQueryHandler<ReadPersonQuery> {
  private logger: Logger
  constructor(private repository: EventStoreRepository) {
    this.logger = new Logger('ReadQueryHandler')
  }

  async execute(query: ReadPersonQuery): Promise<IPerson> {
    this.logger.log('Async ReadHandler...')
    const { id } = query
    
    try {
      return await this.repository.findById(PersonAggregate,'persons-' + id)
    } catch (error) {
      this.logger.error(`Failed to read person of id ${id}`)
      this.logger.log(error.message)
      this.logger.debug(error.stack)
      throw error
    }
  }
}
