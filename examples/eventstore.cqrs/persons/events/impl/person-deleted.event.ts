import { IAggregateEvent } from '../../../../../src'

export class PersonDeletedEvent implements IAggregateEvent {
  constructor(readonly _id: string) {}

  get streamName() {
    return `persons-${this._id}`
  }
}
