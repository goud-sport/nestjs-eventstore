export { PersonAddedEvent } from './person-added.event'
export { PersonDeletedEvent } from './person-deleted.event'