import { IAggregateEvent } from '../../../../../src'

export class PersonAddedEvent implements IAggregateEvent {
  constructor(readonly _id: string, readonly data?: any) {}

  get streamName() {
    return `persons-${this._id}`
  }
}
