import { AddPersonEventHandler } from './add-person.event.handler'
import { DeletePersonEventHandler } from './delete-person.event.handler'

export const PERSON_EVENT_HANDLERS = [AddPersonEventHandler, DeletePersonEventHandler]
