import { EventsHandler, IEventHandler } from '@nestjs/cqrs'
import { Logger } from '@nestjs/common'
import { PersonDeletedEvent } from '../impl'

@EventsHandler(PersonDeletedEvent)
export class DeletePersonEventHandler implements IEventHandler<PersonDeletedEvent> {
  private logger = new Logger('DeleteEventHandler')
  constructor() {}
  async handle(event: PersonDeletedEvent) {
    this.logger.verbose(`EVENT TRIGGERED: ${event.constructor.name}`)
    const { _id } = event
    try {
      // const result = await this.repository.deleteById(_id)
    } catch (error) {
      this.logger.error(`Cannot delete person of id ${_id}`)
      this.logger.log(error.message)
      this.logger.debug(error.stack)
    }
  }
}
