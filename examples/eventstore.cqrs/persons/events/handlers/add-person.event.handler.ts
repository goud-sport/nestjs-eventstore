import { EventsHandler } from '@nestjs/cqrs'
import { Logger } from '@nestjs/common'
import { PersonAddedEvent } from '../impl'
import { IEventHandler, IEventMetadata } from '../../../../../src'

@EventsHandler(PersonAddedEvent)
export class AddPersonEventHandler implements IEventHandler<PersonAddedEvent> {
  private logger = new Logger('AddEventHandler')

  constructor() {}
  async handle(event: PersonAddedEvent, _: IEventMetadata) {
    this.logger.verbose(`EVENT TRIGGERED: ${event.constructor.name}`)
    const { _id } = event
    try {
      // await this.repository.create(data)
    } catch (error) {
      this.logger.error(`Failed to create person of id ${_id}`)
      this.logger.log(error.message)
      this.logger.debug(error.stack)
    }
  }
}
