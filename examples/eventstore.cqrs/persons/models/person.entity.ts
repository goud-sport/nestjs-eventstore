import { Column, Entity, ObjectIdColumn } from 'typeorm'
import { IPerson } from './person.model.interface'

@Entity('person')
export class PersonEntity implements IPerson {
  @ObjectIdColumn()
  _id: string

  @Column()
  email: string

  @Column()
  firstName: string

  @Column()
  lastName: string

  @Column()
  phoneNumber: number

}