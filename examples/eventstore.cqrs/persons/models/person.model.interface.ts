export interface IPerson {
  _id: string
  email: string
  firstName: string
  lastName: string
  phoneNumber: number
}