import { NestFactory } from '@nestjs/core'
import { AppModule } from './eventstore.cqrs/app.module'

const settings = {
  url: 'localhost',
  globalPrefix: 'api',
  port: process.env.port || 3333
}

const bootstrap = async () => {
  const { port, globalPrefix } = settings
  const app = await (await NestFactory.create(AppModule)).setGlobalPrefix(globalPrefix)

  await app.listen(port, () => confirmMessage(settings))
}

const confirmMessage = ({ url, port, globalPrefix }) =>
  console.log(`Listening at http://${url}:${port}/${globalPrefix}`)

bootstrap()
