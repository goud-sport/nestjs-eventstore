import { Test, TestingModule } from '@nestjs/testing'
import { EventBusProvider, EventStoreCqrsModule } from '../src/event-store/eventstore-cqrs'
import { v4 as uuidv4 } from 'uuid'
import { ResolvedEvent } from 'node-eventstore-client'
import { eventStoreConfig } from './test-config'
import { UserCreatedEvent } from './subscriptions.spec'

describe('eventstore functions', () => {
  let eventBusProvider: EventBusProvider
  let module: TestingModule
  let testStream: string
  let testStreamEmpty: string

  beforeEach(async () => {
    module = await Test.createTestingModule({
      imports: [
        EventStoreCqrsModule.forRootAsync(eventStoreConfig, {
          onlyAckWhenHandlerIsDone: true
        })
      ]
    }).compile()

    eventBusProvider = module.get<EventBusProvider>(EventBusProvider)

    testStream = `users-${uuidv4()}`
    testStreamEmpty = `users-${uuidv4()}`
    for (let i = 0; i <= 14; i++) {
      await eventBusProvider.publisher.publish(new UserCreatedEvent(i), testStream)
    }
  })

  afterEach(async () => {
    await eventBusProvider.publisher.deleteStream(testStream, true)
    await eventBusProvider.publisher.deleteStream(testStreamEmpty, true)
    await module.close()
    await eventBusProvider.onModuleDestroy()
  })

  it('getStreamLength should return the correct length', async () => {
    const someStreamLength: number = await eventBusProvider.publisher.getStreamLength(testStreamEmpty)
    const userStreamLength: number = await eventBusProvider.publisher.getStreamLength(testStream)

    expect(someStreamLength).toBe(-1)
    expect(userStreamLength).toBe(15)
  })

  it('getEventsBackward should return correct events', async () => {
    const eventsAll: any[] = await eventBusProvider.publisher.getEventsBackward(testStream)
    const eventsFirst5: any[] = await eventBusProvider.publisher.getEventsBackward(testStream, 4)
    const events3fromposition4: any[] = await eventBusProvider.publisher.getEventsBackward(testStream, 4, 3)

    expect(eventsAll.length).toBe(15)
    expect(eventsFirst5.length).toBe(5)
    expect(events3fromposition4.length).toBe(3)
    expect(eventsAll[0].userId).toBe(14)
    expect(eventsFirst5[0].userId).toBe(4)
    expect(events3fromposition4[0].userId).toBe(4)
  })

  it('getEventsForward should return correct events', async () => {
    const eventsAll: any[] = await eventBusProvider.publisher.getEventsForward(testStream)
    const eventsFrom4: any[] = await eventBusProvider.publisher.getEventsForward(testStream, 4)
    const events3fromposition4: any[] = await eventBusProvider.publisher.getEventsForward(testStream, 4, 3)

    expect(eventsAll.length).toBe(15)
    expect(eventsFrom4.length).toBe(11)
    expect(events3fromposition4.length).toBe(3)
    expect(eventsAll[0].userId).toBe(0)
    expect(eventsFrom4[0].userId).toBe(4)
    expect(events3fromposition4[0].userId).toBe(4)
  })

  it('convertEvent should return correct event type and data', async () => {
    const resolvedEvent: any = {
      event: {
        data: Buffer.from('{ "userId": 20 }'),
        eventType: 'userCreatedEvent'
      }
    }

    const convertedEvent: any = await eventBusProvider.publisher.convertEvent(resolvedEvent as ResolvedEvent)

    expect(convertedEvent).toBeDefined()
    expect(convertedEvent.userId).toBe(20)
    expect(convertedEvent.constructor.name).toBe('userCreatedEvent')
  })
})
