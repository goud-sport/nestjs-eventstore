import { Test, TestingModule } from '@nestjs/testing'
import {
  EventBusProvider,
  EventStoreCqrsModule,
  EventStoreSubscriptionType
} from '../src/event-store/eventstore-cqrs'
import { EventsHandler, IEvent } from '@nestjs/cqrs'
import { v4 as uuidv4 } from 'uuid'
import { eventStoreConfig } from './test-config'
import { IEventHandler } from '../src'
import { Logger } from '@nestjs/common'
import SpyInstance = jest.SpyInstance

export class UserCreatedEvent implements IEvent {
  constructor(readonly userId: number) {}
}

@EventsHandler(UserCreatedEvent)
class UserAddedEventHandler implements IEventHandler<UserCreatedEvent> {
  private logger = new Logger('UserAddedEventHandler')

  async handle(event: UserCreatedEvent): Promise<any> {
    this.logger.verbose(`Recived ${event.constructor.name} with userId ${event.userId}`)
  }
}

describe('idk', () => {
  let eventBusProvider: EventBusProvider
  let module: TestingModule
  let handlerSpy: SpyInstance

  let testStream: string

  beforeEach(async () => {
    testStream =  `users-${uuidv4()}`
    module = await Test.createTestingModule({
      imports: [
        EventStoreCqrsModule.forRootAsync(eventStoreConfig, {
          onlyAckWhenHandlerIsDone: true,
          subscriptions: [
            {
              type: EventStoreSubscriptionType.PERSISTENT,
              stream: testStream,
              synchronize: true,
              persistentSubscriptionName: 'users'
            }
          ]
        })
      ],
      providers: [UserAddedEventHandler]
    }).compile()

    // Because the createTestingModule function doesn't support NestJS' lifecycle hooks, we must call
    // onModuleInit manually to register event handlers (see https://github.com/nestjs/nest/issues/176)
    const eventStoreModule = module.get<EventStoreCqrsModule>(EventStoreCqrsModule)
    eventStoreModule.onModuleInit()

    eventBusProvider = module.get<EventBusProvider>(EventBusProvider)
    handlerSpy = jest.spyOn(UserAddedEventHandler.prototype, 'handle')
  })

  afterEach(async () => {
    await eventBusProvider.publisher.deleteStream(testStream, true)
    await eventBusProvider.publisher.deletePersistantSubscription(testStream, 'users')
    await module.close()
    await eventBusProvider.onModuleDestroy()
    jest.clearAllMocks()
  })

  it('should handle an event from a persistant subscription', async (done) => {
    await eventBusProvider.publisher.publish(new UserCreatedEvent(1), testStream)

    eventBusProvider.subject$.subscribe((event) => {
      const userCreateEvent = event.event as UserCreatedEvent
      expect(userCreateEvent.userId).toBeDefined()
      expect(handlerSpy).toHaveBeenCalledTimes(1)
      done()
    })
  }, 2000)

  it('should handle events from a catchup subscription', async (done) => {
    for (let i = 0; i < 10; i++) {
      await eventBusProvider.publisher.publish(new UserCreatedEvent(i), testStream)
    }

    eventBusProvider.publisher.subscribeToCatchupSubscription(testStream)

    eventBusProvider.subject$.subscribe((event) => {
      const userCreateEvent = event.event as UserCreatedEvent
      expect(userCreateEvent.userId).toBeDefined()

      if (event.eventMetaData.eventNumber === 10) {
        expect(handlerSpy).toHaveBeenCalledTimes(10)
      }
      done()
    })
  }, 2000)

  // TODO
  // it('should create snapshots every N events', async () => {
  //
  // })
})
