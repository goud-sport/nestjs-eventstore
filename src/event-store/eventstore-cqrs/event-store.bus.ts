import { AggregateRoot, IEvent } from '@nestjs/cqrs'
import { Subject } from 'rxjs'
import {
  createEventData,
  EventData,
  EventStoreCatchUpSubscription,
  EventStorePersistentSubscription,
  EventStoreSubscription,
  ResolvedEvent,
  StreamEventsSlice
} from 'node-eventstore-client'
import { v4 } from 'uuid'
import { Logger } from '@nestjs/common'
import { EventStore } from '../event-store.class'
import {
  ESCatchUpSubscription,
  ESPersistentSubscription,
  EventStoreBusConfig,
  EventStoreSnapshotConfig,
  EventStoreSubscriptionGeneral,
  EventStoreSubscriptionType
} from './event-bus.provider'
import { IEventMetadata, PersistentSubscriptionOptions } from '../..'

interface IExtendedCatchUpSubscription extends EventStoreCatchUpSubscription {
  isLive: boolean | undefined
}

interface IExtendedPersistentSubscription extends EventStorePersistentSubscription {
  isLive: boolean | undefined
}

export class EventStoreBus {
  private logger = new Logger('EventStoreBus')
  private catchupSubscriptions: IExtendedCatchUpSubscription[] = []
  private catchupSubscriptionsCount: number

  private persistentSubscriptions: IExtendedPersistentSubscription[] = []
  private persistentSubscriptionsCount: number

  // @ts-ignore
  private volatileSubscriptions: Promise<EventStoreSubscription>[] = []
  // @ts-ignore
  private volatileSubscriptionsCount: number

  constructor(
    private eventStore: EventStore,
    private subject$: Subject<{
      event: IEvent
      eventMetaData: IEventMetadata
      subscription: EventStoreSubscriptionGeneral
      isReplay: boolean
    }>,
    private subscriptionEventNames: string[],
    private config: EventStoreBusConfig
  ) {
    if (!config.subscriptions) return

    const catchupSubscriptions = config.subscriptions.filter((sub) => sub.type === EventStoreSubscriptionType.CATCHUP)
    this.subscribeToCatchUpSubscriptions(catchupSubscriptions as ESCatchUpSubscription[])

    const persistentSubscriptions = config.subscriptions.filter(
      (sub) => sub.type === EventStoreSubscriptionType.PERSISTENT
    )
    this.subscribeToPersistentSubscriptions(persistentSubscriptions as ESPersistentSubscription[])

    const volatileSubscriptions = config.subscriptions?.filter((sub) => sub.type === EventStoreSubscriptionType.VOLATILE)
    this.subscribeToVolatileSubscriptions(volatileSubscriptions as ESCatchUpSubscription[])
  }

  static setConstructorName(payload: any, type: string): any {
    payload.constructor = { name: type }
    return Object.assign(Object.create(payload), payload)
  }

  async subscribeToPersistentSubscriptions(subscriptions: ESPersistentSubscription[]) {
    this.persistentSubscriptionsCount = subscriptions.length
    this.persistentSubscriptions = await Promise.all(
      subscriptions.map(async (subscription) => {
        if (subscription.synchronize) {
          await this._createPersistentSubscription(
            subscription.stream,
            subscription.persistentSubscriptionName,
            subscription.persistentSubscriptionOptions
          )
        }
        return await this.subscribeToPersistentSubscription(
          subscription.stream,
          subscription.persistentSubscriptionName
        )
      })
    )
  }

  private async _createPersistentSubscription(
    stream: string,
    persistentSubscriptionName: string,
    options?: Partial<PersistentSubscriptionOptions>
  ): Promise<void> {
    await this.eventStore.connection
      .createPersistentSubscription(stream, persistentSubscriptionName, new PersistentSubscriptionOptions(options))
      .catch((e) => {
        if (!e.message.includes('already exists')) throw e
      })
    this.logger.log(
      `Succesfully made persistent subscription on stream ${stream} with group name ${persistentSubscriptionName}`
    )
  }

  subscribeToCatchUpSubscriptions(subscriptions: ESCatchUpSubscription[]) {
    this.catchupSubscriptionsCount = subscriptions.length
    this.catchupSubscriptions = subscriptions.map((subscription) => {
      return this.subscribeToCatchupSubscription(subscription.stream)
    })
  }

  subscribeToVolatileSubscriptions(subscriptions: ESCatchUpSubscription[]) {
    this.volatileSubscriptionsCount = subscriptions.length
    this.volatileSubscriptions = subscriptions.map((subscription) => {
      return this.subscribeToVolatileSubscription(subscription.stream)
    })
  }

  get allCatchUpSubscriptionsLive(): boolean {
    const initialized = this.catchupSubscriptions.length === this.catchupSubscriptionsCount
    return (
      initialized &&
      this.catchupSubscriptions.every((subscription) => {
        return !!subscription && subscription.isLive
      })
    )
  }

  get allPersistentSubscriptionsLive(): boolean {
    const initialized = this.persistentSubscriptions.length === this.persistentSubscriptionsCount
    return (
      initialized &&
      this.persistentSubscriptions.every((subscription) => {
        return !!subscription && subscription.isLive
      })
    )
  }

  get isLive(): boolean {
    return this.allCatchUpSubscriptionsLive && this.allPersistentSubscriptionsLive
  }

  async publish(event: IEvent, stream?: string, eventMetaData?: any) {
    const payload: EventData = createEventData(
      v4(),
      event.constructor.name,
      true,
      Buffer.from(JSON.stringify(event)),
      !!eventMetaData ? Buffer.from(JSON.stringify(eventMetaData)) : undefined
    )

    try {
      await this.eventStore.connection.appendToStream(stream, -2, [payload])
    } catch (err) {
      this.logger.error(err.message, err.stack)
    }
  }

  async publishAll(events: IEvent[], stream?: string) {
    try {
      await this.eventStore.connection.appendToStream(
        stream,
        -2,
        (events || []).map((event: IEvent) =>
          createEventData(v4(), event.constructor.name, true, Buffer.from(JSON.stringify(event)))
        )
      )
    } catch (err) {
      this.logger.error(err)
    }
  }

  async getStreamLength(stream: string, defaultValue: number = -1): Promise<number> {
    const event = await this.eventStore.connection.readStreamEventsBackward(stream, -1, 1, false)
    return event.lastEventNumber.low === -1 ? defaultValue : event.lastEventNumber.low + 1
  }

  convertEvent(resolvedEvent: ResolvedEvent): any {
    const obj = JSON.parse(resolvedEvent.event.data.toString())
    return EventStoreBus.setConstructorName(obj, resolvedEvent.event.eventType)
  }

  async getEventsForward(stream: string, start: number = 0, count: number = null): Promise<any[]> {
    count = count ?? (await this.getStreamLength(stream, 1))
    const slice: StreamEventsSlice = await this.eventStore.connection.readStreamEventsForward(
      stream,
      start,
      count,
      true
    )
    return slice.events.map((e) => this.convertEvent(e))
  }

  async getEventsBackward(stream: string, start: number = -1, count: number = null): Promise<any[]> {
    count = count ?? (await this.getStreamLength(stream, 1))
    const slice: StreamEventsSlice = await this.eventStore.connection.readStreamEventsBackward(
      stream,
      start,
      count,
      true
    )
    return slice.events.map((e) => this.convertEvent(e))
  }

  async replayEventsForward(stream: string, start: number = 0, count: number = null): Promise<void> {
    const events = await this.getEventsForward(stream, start, count)
    for (const event of events) {
      this.onEvent(null, event, true)
    }
  }

  async replayEventsBackward(stream: string, start: number = -1, count: number = null): Promise<void> {
    const events = await this.getEventsBackward(stream, start, count)
    for (const event of events) {
      this.onEvent(null, event, true)
    }
  }

  subscribeToCatchupSubscription(stream: string): IExtendedCatchUpSubscription {
    this.logger.log(`Catching up and subscribing to stream ${stream}!`)
    try {
      return this.eventStore.connection.subscribeToStreamFrom(
        stream,
        0,
        true,
        (sub, payload) => this.onEvent(sub, payload),
        (subscription) => this.onLiveProcessingStarted(subscription as IExtendedCatchUpSubscription),
        (sub, reason, error) => this.onDropped(sub as IExtendedCatchUpSubscription, reason, error)
      ) as IExtendedCatchUpSubscription
    } catch (err) {
      this.onDropped(null, err.message, err)
    }
  }

  async subscribeToPersistentSubscription(
    stream: string,
    subscriptionName: string
  ): Promise<IExtendedPersistentSubscription> {
    try {
      this.logger.log(`
      Connecting to persistent subscription ${subscriptionName} on stream ${stream}!
      `)
      const resolved = (await this.eventStore.connection.connectToPersistentSubscription(
        stream,
        subscriptionName,
        (sub, payload) => this.onEvent(sub as EventStorePersistentSubscription, payload),
        (sub, reason, error) => this.onDropped(sub as IExtendedPersistentSubscription, reason, error),
        undefined,
        undefined,
        !this.config.onlyAckWhenHandlerIsDone
      )) as IExtendedPersistentSubscription

      return resolved
    } catch (err) {
      this.onDropped(null, err.message, err)
    }
  }

  async subscribeToVolatileSubscription(stream: string): Promise<EventStoreSubscription> {
    try {
      this.logger.log(`Creating volatile subscription on stream ${stream}!`)
      return await this.eventStore.connection.subscribeToStream(
        stream,
        true,
        (sub, payload) => this.onEvent(sub as EventStoreSubscription, payload),
        (sub, reason, error) => this.onDropped(sub as EventStoreSubscription, reason, error)
      )
    } catch (err) {
      this.onDropped(null, err.message, err)
    }
  }

  onEvent(subscription: EventStoreSubscriptionGeneral, payload: ResolvedEvent, isReplay = false) {
    const { event, originalEvent } = payload

    if ((payload.link !== null && !payload.isResolved) || !event || !event.isJson) {
      this.logger.error(`Received ${event.eventType} that could not be resolved!`)
      return
    }
    // TODO create an ignored-events array in the config
    if (event.eventType.startsWith('$') || event.eventStreamId.endsWith('snapshots')) {
      this.acknowledgeIfNeeded(subscription, originalEvent.eventId)
      this.logger.log(`Received ${event.eventType} will be ignored`)
      return
    }
    if (!this.subscriptionEventNames.some((name) => name === event.eventType)) {
      this.logger.error(`Received ${event.eventType} could not be handled by any of the provided event handlers!`)
      return
    }

    // Make a snapshot asynchronously if the stream name is set in the config at it is the N'th event
    const rollingSnapshot: EventStoreSnapshotConfig = this.config.rollingSnapshots?.find((x) =>
      event.eventStreamId.startsWith(x.name)
    )
    if (rollingSnapshot && event.eventNumber.low > 0 && event.eventNumber.low % rollingSnapshot.everyNEvents === 0) {
      this.createSnapshot(event.eventStreamId, rollingSnapshot.aggegrate)
    }

    let data: any = JSON.parse(event.data.toString())
    data = EventStoreBus.setConstructorName(data, event.eventType)

    const metadata = event.metadata.length > 0 ? JSON.parse(event.metadata.toString()) : null
    const eventMetaData: IEventMetadata = {
      eventName: event.eventType,
      streamMetaData: metadata,
      eventNumber: event.eventNumber.low,
      eventId: event.eventId,
      originalEventId: originalEvent.eventId,
      eventStreamId: event.eventStreamId,
      dateCreated: event.created
    }

    this.subject$.next({ event: data, eventMetaData, subscription, isReplay })

  }

  acknowledgeIfNeeded(subscription: EventStoreSubscriptionGeneral, originalEventId: string) {
    if (!(<EventStorePersistentSubscription>subscription).acknowledge || !this.config.onlyAckWhenHandlerIsDone) {
      return // Only Persistent subscriptions need acknowledgement
    }
    const persistentSubscription = subscription as EventStorePersistentSubscription
    persistentSubscription['_subscription'].notifyEventsProcessed([originalEventId])

    this.logger.log('Ack sent for eventId ' + originalEventId)
  }

  handleExceptionInHandler(exception: any, originalEventId: string) {
    this.logger.error(
      `The handler threw an exception while handling event number ${originalEventId}, exception: \n ${exception}`
    )
  }

  onDropped(
    subscription: IExtendedPersistentSubscription | IExtendedCatchUpSubscription | EventStoreSubscription,
    reason: string,
    error: Error
  ) {
    if ('isLive' in subscription) {
      subscription.isLive = false
    }
    if (reason !== "connectionClosed") {
      this.logger.error(error.message + "Reason: " + reason)
    }
  }

  onLiveProcessingStarted(subscription: IExtendedCatchUpSubscription) {
    subscription.isLive = true
    this.logger.log('Live processing of EventStore events started!')
  }

  /**
   * You probably should only use this method for testing purposes
   * @param stream
   * @param hardDelete
   */
  async deleteStream(stream: string, hardDelete = false): Promise<void> {
    await this.eventStore.connection.deleteStream(stream, -2, hardDelete)
  }

  async deletePersistantSubscription(stream: string, groupName: string) {
    await this.eventStore.connection.deletePersistentSubscription(stream, groupName)
  }

  async createSnapshot(eventStreamId: string, aggregate: new () => AggregateRoot) {
    const snapshotStreamId = eventStreamId + '-snapshots'
    const lastSnapshot = await this.eventStore.connection.readStreamEventsBackward(snapshotStreamId, -1, 1, false)
    const lastEventNr: number = await this.getStreamLength(eventStreamId)

    let aggegrateObj: AggregateRoot = new aggregate()
    let start = 0

    // Get latest aggregate from snapshot if it exists
    if (lastSnapshot.events.length > 0) {
      const snapshotLatest = lastSnapshot.events[0]
      const data = JSON.parse(snapshotLatest.event.data.toString())
      const metadata = JSON.parse(snapshotLatest.event.metadata.toString())

      aggegrateObj = Object.assign(new aggregate(), data)
      start = metadata?.latestEventNr ?? 0
    }

    const events: any[] = await this.getEventsForward(eventStreamId, start)
    aggegrateObj.loadFromHistory(events)

    await this.publish(aggegrateObj, snapshotStreamId, { lastEventNr })
  }
}
