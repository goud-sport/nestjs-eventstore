import { Injectable, OnModuleDestroy, Type } from '@nestjs/common'
import { ModuleRef } from '@nestjs/core'
import { Observable, Subscription } from 'rxjs'
import { filter } from 'rxjs/operators'
import { AggregateRoot, CommandBus, IEvent, InvalidSagaException, ISaga, ObservableBus } from '@nestjs/cqrs'
import { EVENTS_HANDLER_METADATA, SAGA_METADATA } from '@nestjs/cqrs/dist/decorators/constants'
import { EventStoreBus } from './event-store.bus'
import { EventStore } from '../event-store.class'
import { IEventHandler, IEventMetadata, PersistentSubscriptionOptions } from '../..'
import {
  EventStoreCatchUpSubscription,
  EventStorePersistentSubscription,
  EventStoreSubscription
} from 'node-eventstore-client'

export enum EventStoreSubscriptionType {
  PERSISTENT,
  CATCHUP,
  VOLATILE
}

export type ESPersistentSubscription = {
  type: EventStoreSubscriptionType.PERSISTENT
  stream: string
  persistentSubscriptionName: string
  synchronize: boolean
  persistentSubscriptionOptions?: Partial<PersistentSubscriptionOptions>
}

export type ESCatchUpSubscription = {
  type: EventStoreSubscriptionType.CATCHUP
  stream: string
}

export type ESVolatileSubscription = {
  type: EventStoreSubscriptionType.VOLATILE
  stream: string
}

export type ESSubscription = ESPersistentSubscription | ESCatchUpSubscription | ESVolatileSubscription

export type EventStoreSubscriptionGeneral =
  | EventStorePersistentSubscription
  | EventStoreCatchUpSubscription
  | EventStoreSubscription

export type EventStoreSnapshotConfig = {
  name: string
  everyNEvents: number
  aggegrate: new () => AggregateRoot
}
export type EventStoreBusConfig = {
  onlyAckWhenHandlerIsDone: boolean
  subscriptions?: ESSubscription[]
  rollingSnapshots?: Array<EventStoreSnapshotConfig>
}

export type EventHandlerType = Type<IEventHandler<IEvent>>

@Injectable()
export class EventBusProvider
  extends ObservableBus<{
    event: IEvent
    eventMetaData: IEventMetadata
    subscription: EventStoreSubscriptionGeneral
    isReplay: boolean
  }>
  implements OnModuleDestroy {
  constructor(
    private readonly commandBus: CommandBus,
    private readonly moduleRef: ModuleRef,
    private readonly eventStore: EventStore,
    private config: EventStoreBusConfig
  ) {
    super()
    this.subscriptions = []
    this._useDefaultPublisher()
  }

  get publisher(): EventStoreBus {
    return this._publisher
  }

  set publisher(_publisher: EventStoreBus) {
    this._publisher = _publisher
  }

  private _publisher: EventStoreBus
  private readonly subscriptions: Subscription[]
  private readonly subscriptionEventNames: string[] = []

  private static _reflectEventsNames(handler: EventHandlerType): FunctionConstructor[] {
    return Reflect.getMetadata(EVENTS_HANDLER_METADATA, handler)
  }

  onModuleDestroy() {
    this.subscriptions.forEach((subscription) => subscription.unsubscribe())
    this.eventStore.close()
  }

  publish<T extends IEvent>(event: T, stream: string) {
    this._publisher.publish(event, stream)
  }

  publishAll(events: IEvent[]) {
    (events || []).forEach((event) => this._publisher.publish(event))
  }

  bind(handler: IEventHandler<IEvent>, name: string) {
    const subscription = this._ofEventName(name).subscribe(async (event) => {
      try {
        await handler.handle(event.event, event.eventMetaData, event.isReplay)
        if (event.subscription)
          this.publisher.acknowledgeIfNeeded(event.subscription, event.eventMetaData.originalEventId)
      } catch (e) {
        this.publisher.handleExceptionInHandler(e, event.eventMetaData.originalEventId)
      }
    })
    this.subscriptionEventNames.push(name)
    this.subscriptions.push(subscription)
  }

  registerSagas(types: Type<any>[] = []) {
    const sagas = types
      .map((target) => {
        const metadata = Reflect.getMetadata(SAGA_METADATA, target) || []
        const instance = this.moduleRef.get(target, { strict: false })
        if (!instance) {
          throw new InvalidSagaException()
        }
        return metadata.map((key: string) => instance[key])
      })
      .reduce((a, b) => a.concat(b), [])

    sagas.forEach((saga) => this._registerSaga(saga))
  }

  register(handlers: EventHandlerType[] = []) {
    handlers.forEach((handler) => this._registerHandler(handler))
  }

  protected _registerHandler(handler: EventHandlerType) {
    const instance = this.moduleRef.get(handler, { strict: false })
    if (!instance) {
      return
    }
    const eventsNames = EventBusProvider._reflectEventsNames(handler)
    eventsNames.map((event) => this.bind(instance as IEventHandler<IEvent>, event.name))
  }

  protected _ofEventName(name: string) {
    return this.subject$.pipe(filter((event) => event.eventMetaData.eventName === name))
  }

  protected _registerSaga(saga: ISaga) {
    if (!(saga instanceof Function)) {
      throw new InvalidSagaException()
    }
    const stream$ = saga(this)
    if (!(stream$ instanceof Observable)) {
      throw new InvalidSagaException()
    }

    const subscription = stream$.pipe(filter((e) => !!e)).subscribe((command) => this.commandBus.execute(command))

    this.subscriptions.push(subscription)
  }

  private _useDefaultPublisher() {
    this._publisher = new EventStoreBus(this.eventStore, this.subject$, this.subscriptionEventNames, this.config)
  }
}
