import {
  ConnectionSettings,
  createConnection,
  EventStoreNodeConnection,
  TcpEndPoint,
  UserCredentials
} from 'node-eventstore-client'
import { Logger } from '@nestjs/common'

export class EventStore {
  connection: EventStoreNodeConnection
  isConnected = false
  logger: Logger = new Logger(this.constructor.name)

  constructor(private settings: ConnectionSettings, private endpoint: TcpEndPoint) {
    this.connect()
  }

  async connect() {
    try {
      this.settings.defaultUserCredentials = new UserCredentials(this.settings['username'], this.settings['password'])
      this.connection = createConnection(this.settings, this.endpoint)
      this.connection.connect()
      this.connection.on('connected', () => {
        this.logger.log('Connection to EventStore established!')
        this.isConnected = true
      })
      this.connection.on('closed', (reason) => {
        this.logger.error('Connection to EventStore closed! Reason: ' + reason)
        this.isConnected = false

        if (reason !== "Connection close requested by client.") {
          this.logger.verbose('Reconnecting...')
          this.connect()
        }
      })
    } catch (e) {
      this.logger.error(
        `Connection to the event store failed using { connectionSettings: ${JSON.stringify(
          this.settings
        )}, endpoint: ${JSON.stringify(this.endpoint)} }`
      )
      throw e
    }
  }

  close() {
    this.connection.close()
  }
}
