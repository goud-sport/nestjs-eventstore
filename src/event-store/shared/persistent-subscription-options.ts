export enum ConsumerStrategy {
  DISPATCH_TO_SINGLE = 'DispatchToSingle',
  PINNED = 'Pinned',
  ROUND_ROBIN = 'RoundRobin'
}

export class PersistentSubscriptionOptions {
  resolveLinkTos = true
  startFrom = 0
  extraStatistics = true
  messageTimeout = 10000
  maxRetryCount = 10
  liveBufferSize = 500
  readBatchSize = 20
  historyBufferSize = 500
  checkPointAfter = 1000
  maxCheckPointCount = 500
  minCheckPointCount = 5
  maxSubscriberCount = 10
  namedConsumerStrategy: ConsumerStrategy = ConsumerStrategy.ROUND_ROBIN

  constructor(opt?: Partial<PersistentSubscriptionOptions>) {
    Object.assign(this, opt)
  }
}
