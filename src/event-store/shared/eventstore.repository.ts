import { Inject, NotFoundException } from '@nestjs/common'
import { AggregateRoot, EventBus } from '@nestjs/cqrs'
import { EventBusProvider } from '../../index'

export class EventStoreRepository {
  constructor(@Inject(EventBus) private _eventStoreBus: EventBusProvider) {}

  async findById<T extends AggregateRoot>(type: new () => T, streamName: string): Promise<T> {
    const events = await this._eventStoreBus.publisher.getEventsForward(streamName) // TODO why does getEventsForward() return 0 events if stream is at #20
    if (events.length === 0) throw new NotFoundException()

    const person = new type()
    person.loadFromHistory(events)
    return person
  }
}
