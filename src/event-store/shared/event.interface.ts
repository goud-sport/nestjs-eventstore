export interface IEventMetadata {
  eventName: string
  streamMetaData?: any
  eventNumber: number
  eventId: string
  originalEventId: string
  eventStreamId: string
  dateCreated: Date
}
