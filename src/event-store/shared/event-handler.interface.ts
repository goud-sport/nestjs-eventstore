import { IEvent } from '@nestjs/cqrs'
import { IEventMetadata } from './event.interface'

export interface IEventHandler<T extends IEvent = any> {
  handle(event: T, metadata: IEventMetadata, isReplay: boolean): Promise<any>
}
